#data#
data "aws_ami" "nginxec2_ami" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-gp2"]
  }
}

#resources#
#networking#
#vpc#
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = var.enable_dns_hostnames
  tags                 = local.common_tags
}

#gateway#
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags   = local.common_tags
}

#subnet#
resource "aws_subnet" "subnet1" {
  cidr_block              = var.vpc_subnet1_cidr_block
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags                    = local.common_tags
}

#routing#
#routetable#
#traffic goes out by giving default gateway#
resource "aws_route_table" "rtb" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = local.common_tags
}

#routetable association#
#association between route table and subnet#
resource "aws_route_table_association" "rta-subnet1" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.rtb.id
}

#security group#
#nginx security group to allow port 80#
resource "aws_security_group" "nginx-sg" {
  name   = "nginx_sg"
  vpc_id = aws_vpc.vpc.id

  #http access from anywhere into ec2 server#
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #to allow ssh into ec2 instance
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "ssh"
    cidr_blocks = [var.vpc_cidr_block]
  }

  #outbound internet access from ec2 to outside anywhere#
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = local.common_tags
}

#instances#
resource "aws_instance" "nginx" {
  ami                    = data.aws_ami.nginxec2_ami.id
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.subnet1.id
  vpc_security_group_ids = [aws_security_group.nginx-sg.id]
  user_data              = <<EOF
    #! /bin/bash
    sudo amazon-linux-extras install -y nginx
    sudo service nginx start
    sudo rm /usr/share/nginx/html/index.html
    echo '<html><head><title> First amazon remote ec2 server with public subnet port 80 </title></head><body style=\background-color: #1F778D\"> </body>
    EOF
  tags                   = local.common_tags
}
